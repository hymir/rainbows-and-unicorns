/*
 * Copyright (C) 2015 Armin Leghissa
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package at.jku.cp.ai.search.algorithms;

import at.jku.cp.ai.search.AdversarialSearch;
import at.jku.cp.ai.search.Node;
import at.jku.cp.ai.search.datastructures.Pair;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.TreeSet;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Stream;

public class MinMaxSearch implements AdversarialSearch {
	private final BiPredicate<Integer, Node> slp;
	private final TreeSet<Pair<Node, Double>> children;
	private final Stack<Node> path;
	private final Stack<Pair<Node, Double>> backtrace;
	public static Comparator<Pair<Node, Double>> comp = (a, b) -> {
			if (a.s < b.s) {
				return -1;
			} else if (a.s > b.s) {
				return 1;
			} else {
				return 0;
			}
		};
	private int depth = 0;

	/**
	 * To limit the extent of the search, this implementation should honor a
	 * limiting predicate. The predicate returns 'true' as long as we are below
	 * the limit, and 'false', if we exceed the limit.
	 *
	 * @param slp
	 */
	public MinMaxSearch(BiPredicate<Integer, Node> slp) {
		this.path = new Stack<>();
		this.backtrace = new Stack<>();
		this.slp = slp;
		this.children = new TreeSet<>(comp);
	}

	private boolean amimaxx() {
		return depth % 2 == 1;
	}

	private boolean terminal(Node node) {
		return !slp.test(depth, node) || node.isLeaf();
	}

	private void collapse() {
		if (backtrace.empty() || backtrace.peek().s == null) {
			return;
		}

		Pair<Node, Double> mm = minmax();
		Pair<Node, Double> parent = backtrace.pop();
		backtrace.push(new Pair<>(parent.f, mm.s));
	}

	private Pair<Node, Double> minmax() {
		while (backtrace.peek().s != null) {
			children.add(backtrace.pop());
		}

		Pair<Node, Double> mm;

		if (amimaxx()) {
			mm = children.last();
		} else {
			mm = children.first();
		}

		children.clear();
		depth--;
		return mm;
	}

	private Pair<Node, Double> best() {
		Pair<Node, Double> mm;
		Stream.Builder<Pair<Node, Double>> sb = Stream.builder();

		while (backtrace.peek().s != null) {
			sb.add(backtrace.pop());
		}

		if (amimaxx()) {
			mm = sb.build().max(comp).get();
		} else {
			mm = sb.build().min(comp).get();
		}

		depth--;
		return mm;
	}

	public Pair<Node, Double> search(Node start, Function<Node, Double> utility) {
		path.push(start);

		while (!path.isEmpty()) {
			System.out.println("====================== NEW LOOP ======================");
			Node cur = path.pop();
			System.out.printf("SS current Node: %s\n", cur.toString());
			System.out.printf("SS depth: %d\n", depth);
			System.out.printf("SS stack: %s\n", path.toString());

			if (terminal(cur)) {
				backtrace.push(new Pair<>(cur, utility.apply(cur)));
				System.out.printf("TT backtrace: %s\n", backtrace.toString());
			} else {
				if(depth > 1 && backtrace.peek().f.parent() != cur.parent())
					collapse();
				backtrace.push(new Pair<>(cur, null));
				System.out.printf("NN backtrace: %s\n", backtrace.toString());
				System.out.printf("NN children of cur: %s\n", cur.adjacent().toString());
//				List<Node> c = cur.adjacent();
//				int s = c.size();
//				depth += Stream.generate(c.listIterator(s)::previous)
				depth += cur.adjacent().stream()
					//.limit(s)
					.filter(n -> !n.equals(cur)) // why the fuck do i have to filterit??
					.map(n -> path.push(n))
					.map(n -> 1)
					.reduce(0, (b1, b2) -> b1 | b2);
			}
		}
		
		System.out.println("====================== END LOOP ======================");
		System.out.printf("E1 depth: %d\n", depth);
		System.out.printf("E1 stack: %s\n", path.toString());
		System.out.printf("E1 backtrace: %s\n", backtrace.toString());
		while (depth > 1) {
			collapse();
		}
		
		System.out.printf("E2 depth: %d\n", depth);
		System.out.printf("E2 stack: %s\n", path.toString());
		System.out.printf("E2 backtrace: %s\n", backtrace.toString());

		Pair<Node, Double> res;

		if (depth == 1) {
			res = minmax();
		} else {
			res = backtrace.pop();
		}
		
		System.out.printf("RR result: %s\n", res.toString());
		System.out.printf("RR depth: %d\n", depth);
		System.out.printf("RR stack: %s\n", path.toString());
		System.out.printf("RR backtrace: %s\n", backtrace.toString());

		backtrace.clear();
		return res;
	}
}
