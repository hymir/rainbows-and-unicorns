/*
 * Copyright (C) 2015 Armin Leghissa
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package at.jku.cp.ai.search.algorithms;

import at.jku.cp.ai.search.Node;
import at.jku.cp.ai.search.Search;
import at.jku.cp.ai.search.datastructures.Pair;
import at.jku.cp.ai.search.datastructures.StablePriorityQueue;
import java.util.HashSet;
import java.util.function.Function;
import java.util.function.Predicate;


/**
 * Uniform Cost Search algorithm.
 *
 * @author Armin Leghissa
 */
public class UCS implements Search {
	private final Function<Node, Double> cost;
	private final StablePriorityQueue<Double, Node> pqueue;
	private final HashSet<Node> visited;


	/**
	 * Initialise all data structures by providing a cost {@link Function}.
	 *
	 * @param cost A {@link Function} for retrieving the costs of a node.
	 */
	public UCS(Function<Node, Double> cost) {
		this.cost = cost;
		this.pqueue = new StablePriorityQueue<>();
		this.visited = new HashSet<>();
	}


	/**
	 * The search function.
	 *
	 * @param start {@link Node} to start search on.
	 * @param isEnd A search criteria.
	 * @return the node obeying the given condition or null if not found.
	 */
	@Override
	public Node search(Node start, Predicate<Node> isEnd) {

		pqueue.add(new Pair<>(cost.apply(start), start));
		visited.add(start);

		// empty fifo is termination condition so start-node must be added initially.
		while (!pqueue.isEmpty()) {
			Pair<Double, Node> curr = pqueue.poll();

			// does current node match our search criteria?
			if (isEnd.test(curr.s)) {
				reset();
				return curr.s;
			} else {
				// expand curr node and add children if existent as long not yet seen.
				if (!curr.s.isLeaf()) {
					curr.s.adjacent().stream()
							.filter(n -> !visited.contains(n))
							.forEach(n -> { // add node with cumulated costs to queue.
								pqueue.offer(new Pair<>(cost.apply(n) + curr.f, n));
								visited.add(n);
							});
				}
			}
		}

		reset();
		return null;
	}


	/**
	 * Give GC a pusch to free unused memory as soon as possible.
	 */
	private void reset() {
		this.pqueue.clear();
		this.visited.clear();
	}
}
