/*
 * Copyright (C) 2015 Armin Leghissa
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package at.jku.cp.ai.search.algorithms;

import at.jku.cp.ai.search.Node;
import at.jku.cp.ai.search.Search;
import at.jku.cp.ai.search.datastructures.StackWithFastContains;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Stack;
import java.util.function.Predicate;


/**
 * Depth-Limited Depth-First Search.
 * Iterative implementation using a depth level stack.
 *
 * @author Armin Leghissa
 */
public class DLDFS implements Search {
	// we need an O(1) datastructure for path-avoidance.
	// 'contains' is O(N) in a stack, where N
	// is the current depth, so we use a stack and a set in parallel
	private final StackWithFastContains<Node> path;
	private final int limit;
	private final HashSet<Node> visited;
	private final Stack<Treshold> depth;


	/**
	 * Initialise the algorithm with needed data structures by given depth limit.
	 *
	 * @param limit The depth limit.
	 */
	public DLDFS(int limit) {
		this.limit = limit;
		this.path = new StackWithFastContains<>();
		this.visited = new HashSet<>();
		this.depth = new Stack<>();
	}


	/**
	 * Search function for DLDFS.
	 *
	 * @param start {@link Node} to start the search at.
	 * @param isEnd Condition for finding the goal.
	 * @return the wanted node if found within the depth limit, else null.
	 */
	@Override
	public Node search(Node start, Predicate<Node> isEnd) {
		path.push(start);

		while (!path.isEmpty()) {
			Node curr = path.pop();
			visited.add(curr);

			if (isEnd.test(curr))
				return curr;

			if (!(curr.isLeaf() || depth.size() >= limit)) {
				// try to expand children
				// for a correct ordering in the stack, we need a reverse-iterator.
				List<Node> children = curr.adjacent();
				ListIterator<Node> it = children.listIterator(children.size());
				int pushes = 0;

				while (it.hasPrevious()) {
					Node n = it.previous();

					if (!(path.contains(n) || visited.contains(n))) {
						path.add(n); // instead of push(), for a correct ordering.
						pushes++;
					}
				}

				if (pushes > 0) {
					depth.push(new Treshold(pushes));
					continue; // children expanded, we don't want depth recovering
				}
			}

			// depth recovering, no children were expanded
			if (depth.peek().t > 1)
				depth.peek().t--;
			else
				depth.pop();
		}

		return null;
	}


	/**
	 * Treshold for depth level decreasing.
	 * The depth itself is given by the stack size.
	 * A decrease treshold of a specific depth level is given
	 * by the number of yet unvisitd children pushed to the path stack.
	 */
	private static class Treshold {
		public int t;


		/**
		 * Init treshold by specifying its value.
		 *
		 * @param t treshold.
		 */
		Treshold(int t) {
			this.t = t;
		}
	}
}
