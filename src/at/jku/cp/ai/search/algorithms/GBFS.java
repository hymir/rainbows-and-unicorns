

package at.jku.cp.ai.search.algorithms;

import at.jku.cp.ai.search.Node;
import at.jku.cp.ai.search.Search;
import at.jku.cp.ai.search.datastructures.Pair;
import at.jku.cp.ai.search.datastructures.StablePriorityQueue;
import java.util.HashSet;
import java.util.function.Function;
import java.util.function.Predicate;

// Greedy Best-First Search

public class GBFS implements Search {
	private Function<Node, Double> heuristic;
	private StablePriorityQueue<Double, Node> pqueue;
	private final HashSet<Node> visited;


	public GBFS(Function<Node, Double> heuristic) {
		this.heuristic = heuristic;
		this.pqueue = new StablePriorityQueue<>();
		this.visited = new HashSet<>();
	}


	@Override
	public Node search(Node start, Predicate<Node> endPredicate) {
		pqueue.add(new Pair<>(0d, start));
		visited.add(start);

		while (!pqueue.isEmpty()) {
			Node curr = pqueue.poll().s;
			pqueue.clear();

			if (endPredicate.test(curr))
				return curr;

			for (Node n : curr.adjacent()) {
				if (!visited.contains(n)) {
					visited.add(n);
					pqueue.add(new Pair<>(heuristic.apply(n), n));
				}
			}
		}

		return null;
	}
}
