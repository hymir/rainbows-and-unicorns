/* 
 * Copyright (C) 2015 Armin Leghissa
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package at.jku.cp.ai.search.algorithms;

import at.jku.cp.ai.search.Node;
import at.jku.cp.ai.search.Search;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Predicate;


/**
 * Breadth first search.
 *
 * @author Armin Leghissa <leghissa@fim.uni-passau.de>
 */
public class BFS implements Search {
	private final HashSet<Node> visited;
	private final Queue<Node> fifo;


	/**
	 * Initialise all needed data structures.
	 */
	public BFS() {
		super();
		this.visited = new HashSet<>();
		this.fifo = new LinkedList<>();
	}


	/**
	 * The search function.
	 *
	 * @param start {@link Node} to start search on.
	 * @param isEnd A search criteria.
	 * @return the node obeying the given condition or null if not found.
	 */
	@Override
	public Node search(Node start, Predicate<Node> isEnd) {

		fifo.add(start);
		visited.add(start);

		// empty fifo is termination condition so start-node must be added initially.
		while (!fifo.isEmpty()) {
			Node curr = fifo.poll();

			// does current node match our search criteria?
			if (isEnd.test(curr)) {
				reset();
				return curr;
			} else {
				// expand curr node and add children if existent as long not yet seen.
				if (!curr.isLeaf()) {
					curr.adjacent().stream()
							.filter(n -> !visited.contains(n))
							.forEach(n -> {
								fifo.offer(n);
								visited.add(n);
							});
				}
			}
		}

		reset();
		return null;
	}


	/**
	 * Give GC a pusch to free unused memory as soon as possible.
	 */
	private void reset() {
		this.fifo.clear();
		this.visited.clear();
	}
}
